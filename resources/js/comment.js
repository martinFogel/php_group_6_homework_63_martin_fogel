$(document).ready(function () {

    $('#create-comment-btn').on('click', function (e) {
        e.preventDefault();

        const postId = $('#post_id').val();

        $.ajax({
            method: 'POST',
            url: `/posts/${postId}/comments`,
            data: $('#create-comment').serialize(),
        }).done(response => {
            console.log('Success => ', response);
            renderComment(response.comment);
        }).fail(response => {
            console.log(response);
        });
    });

    const renderComment = comment => {
        const commentsBlock = $('.scrollit');
        $(commentsBlock).prepend(comment);
        resetForm();
    };

    const resetForm = () => {
        $('#create-comment').trigger('reset');
    }

});
