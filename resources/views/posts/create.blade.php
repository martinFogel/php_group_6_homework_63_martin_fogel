@extends('layouts.app')

@section('content')
    <h1>Create post</h1>
    <form enctype="multipart/form-data" method="post" action="{{ route('posts.store') }}">
        @csrf
        <div class="form-group">
            <label for="body">Body</label>
            <textarea class="form-control @error('body') is-invalid @enderror" id="body"
                      name="body"></textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <div class="custom-file">
                <input type="file" class="custom-file-input @error('picture') is-invalid @enderror" id="customFile"
                       name="picture" value="picture">
                @error('picture')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
