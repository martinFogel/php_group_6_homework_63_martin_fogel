@extends('layouts.app')

@section('content')
    <h2 class="pt-md-5">Posts of users that you are following:</h2>
    <a href="{{route('posts.create')}}" type="button" class="btn btn-outline-primary">
        Add post
    </a>
    <div class="row">
        @foreach($posts as $post)
            <div class="card m-3" style="width: 18rem;">
                <img src="{{asset('/storage/' . $post->picture)}}" class="card-img-top"
                     style="height: 290px; width: auto" alt="{{asset('/storage/' . $post->picture)}}"
                     width="50px" height="50px">
                <div class="card-body">
                    <p>{{$post->likes->count()}} Likes</p>
                    <strong>Author: {{$post->user->name}}</strong>
                    <p>{{$post->body}}</p>
                    @can('delete', $post)
                        <form method="post" action="{{route('posts.destroy', ['post' => $post])}}">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    @endcan
                    @can('like', $post)
                    <form method="post" action="{{route('likes.store')}}">
                        <input name="post_id" type="hidden" id="post_id" value="{{$post->id}}">
                        @csrf
                        <button type="submit" class="btn btn-success">Like</button>
                    </form>
                        @endcan
                        <a href="{{route('posts.show', ['post' => $post])}}" class="btn btn-primary">More</a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
