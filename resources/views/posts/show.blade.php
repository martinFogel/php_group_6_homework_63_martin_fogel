@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="border-bottom pb-3 mb-3">
            <div class="row row-cols-1 row-cols-md-2 row-cols-lg-2 .img-fluid. w-100%  h-auto ">
                <div class="col mb-4">
                    <br>
                    <h4>Post:</h4>
                    @if (!is_null($post->body))
                        <p>{{$post->body}}</p>
                    @else
                        Нет описания
                    @endif
                </div>
                <div class="col mb-1 ">
                    <img src="{{asset('/storage/' . $post->picture)}}"
                         class="rounded mx-auto d-block img-fluid rounded float-right"
                         alt="{{asset('/storage/' . $post->picture)}}" width="200px" height="200px">
                </div>
                <footer class="blockquote-footer">
                    Author: {{$post->user->name}}, created in
                </footer>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h3>Comments</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-8 scrollit">
                @foreach($post->comments as $comment)
                    @include('comments.comment', ['comment' => $comment])
                @endforeach
            </div>
            <div class="col-4 fixed">
                <div class="comment-form">
                    <form id="create-comment">
                        @csrf
                        <input type="hidden" id="post_id" value="{{$post->id}}">
                        <div class="form-group">
                            <label for="commentFormControl">Comment</label>
                            <textarea name="body" class="form-control" id="commentFormControl" rows="3"
                                      required></textarea>
                        </div>
                        <button id="create-comment-btn" type="submit" class="btn btn-outline-primary btn-sm btn-block">
                            Add new
                            comment
                        </button>
                    </form>
                </div>
            </div>
        </div>
@endsection
