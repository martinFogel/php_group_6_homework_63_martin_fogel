@extends('layouts.app')


@section('content')

    <div style="padding-bottom: 30px;">
        <h1>All users</h1>
    </div>

    <table class="table" style="padding-top: 30px">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            @if(Auth::user()->id != $user->id)
                <tr>
                    <td>
                        {{$user->name}}
                    </td>
                    <td>
                        @if((\App\Models\Follow::where('follow_id', '=', $user->id)->exists() && \App\Models\Follow::where('follower_id', '=', Auth::user()->id)->exists()) !== true)
                        <form method="post" action="{{route('follows.store')}}">
                            <input name="follow_id" type="hidden" id="follow_id" value="{{$user->id}}">
                            @csrf
                            <button type="submit" class="btn btn-success">Follow</button>
                        </form>
                        @elseif((\App\Models\Follow::where('follow_id', '=', $user->id)->exists() && \App\Models\Follow::where('follower_id', '=', Auth::user()->id)->exists()) === true)
                            <form method="post" action="{{route('follows.destroy', ['follow' => $user])}}">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger">Unfollow</button>
                            </form>
                            @endif
                            @else

                        @endif
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>

@endsection
