@extends('layouts.app')

@section('content')
    <h1>Редактировать комментарий</h1>
    <form method="post" action="{{ route('comments.update', ['comment' => $comment]) }}">
        @method('put')
        @csrf
        <div class="form-group">
            <label for="description">Комментарий</label>
            <textarea class="form-control  @error('body') is-invalid @enderror" id="body"
                      name="body">{{$comment->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Обновить</button>
    </form>
@endsection
