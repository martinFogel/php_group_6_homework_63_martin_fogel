<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture' => ['image', 'required'],
            'body' => ['required', 'max:500']
        ];
    }

    public function messages(): array
    {
        return [
            'picture.image' => 'Неправильный формат фото::attribute',
            'body.required' => 'Нужно ввести данные в поле: :attribute',
            'body.max' => 'Максимальное число символов 500: :attribute',
        ];
    }
}
