<?php

namespace App\Http\Form;

use App\Models\Post;
use Illuminate\Http\Request;

class PostForm extends Form
{

    /**
     * @param Request $request
     * @return mixed
     */
    protected function handle(Request $request)
    {
        $data = $request->all();
        $data['user_id'] = $request->user()->id;
        $file = $request->file('picture');
        if (!is_null($file)) {
            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;
        }
        return Post::create($data);
    }
}
