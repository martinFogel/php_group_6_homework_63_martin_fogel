<?php

namespace App\Http\Controllers;

use App\Models\Follow;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UserFollowsController extends Controller
{
    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $follow = new Follow();
        $follow->follower_id = auth()->user()->id;
        $follow->follow_id = $request->input('follow_id');
        $follow->save();
        return redirect()->route('users.index');
    }

    /**
     * @param Follow $follow
     * @return RedirectResponse
     */
    public function destroy(User $user): RedirectResponse
    {
        auth()->user()->follows()->detach($user->id);
        return redirect()->route('users.index');
    }
}
