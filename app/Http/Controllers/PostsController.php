<?php

namespace App\Http\Controllers;

use App\Http\Form\PostForm;
use App\Http\Requests\PostRequest;
use App\Models\Follow;
use App\Models\Post;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $allPosts = Post::all();
        $follows = Follow::where('follower_id', auth()->user()->id)->get();
        $posts = [];
        foreach ($allPosts as $post) {
            foreach ($follows as $follow) {
                if ($post->user_id == $follow->follow_id) {
                    $posts[] = $post;
                }
            }
        }
        return view('posts.index', compact('posts'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * @param PostRequest $request
     * @return RedirectResponse
     */
    public function store(PostRequest $request): RedirectResponse
    {
        $post = PostForm::execute($request);
        $post->user()->associate($request->user());
        $post->save();
        return redirect()->route('posts.index')->with('status', "Post successfully created!");
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function destroy(Post $post): RedirectResponse
    {
        $post->delete();
        return redirect()->route('posts.index')->with('status', "Post successfully deleted!");
    }
}
