<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class CommentsController extends Controller
{
    /**
     * @param Request $request
     * @param Post $post
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Post $post): \Illuminate\Http\JsonResponse
    {
        $request->validate([
            'body' => 'required|min:5',
        ]);
        $comment = new Comment();
        $comment->user_id = $request->user()->id;
        $comment->body = $request->input('body');
        $comment->post_id = $post->id;
        $comment->save();
        return response()->json(
            [
                'comment' => view('comments.comment', compact('comment'))->render()
            ], 201);
    }

    /**
     * @param Comment $comment
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Comment $comment)
    {
        return view('comments.edit', compact('comment'));
    }

    /**
     * @param Request $request
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function update(Request $request, Comment $comment): RedirectResponse
    {
        $request->validate([
            'body' => 'required|min:5'
        ]);
        $comment->update($request->all());
        return redirect()->route('posts.show', $comment->post_id);
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     */
    public function destroy(Comment $comment): RedirectResponse
    {
        $comment->delete();
        return redirect()->route('posts.show', $comment->post_id);
    }
}
