<?php

namespace App\Http\Controllers;

use App\Models\Like;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LikesController extends Controller
{

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if (Like::where('post_id', $request->input('post_id'))->exists() && Like::where('user_id', auth()->user()->id)->exists()) {
            foreach (Like::all() as $like) {
                if ($like->post_id == $request->input('post_id') && $like->user_id == auth()->user()->id) {
                    $like->delete();
                    return redirect()->route('posts.index', $like->post_id);
                }
            }
        } else {
            $like = new Like();
            $like->user_id = auth()->user()->id;
            $like->post_id = $request->input('post_id');
            $like->save();
        }
        return redirect()->route('posts.index');
    }
}
