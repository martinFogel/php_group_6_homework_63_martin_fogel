<?php

use App\Http\Controllers\CommentsController;
use App\Http\Controllers\LikesController;
use App\Http\Controllers\UserFollowsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\PagesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostsController::class, 'index'])->name('home')->middleware(['auth']);

Route::resource('posts', PostsController::class)->middleware(['auth']);

Route::resource('posts.comments', CommentsController::class)
    ->only(
        [
            'store', 'update',
        ]
    )->middleware(['auth']);
Route::resource('users.follows', CommentsController::class)->middleware(['auth']);
Route::resource('comments', CommentsController::class)->middleware(['auth']);
Route::resource('pages', PagesController::class)->middleware(['auth']);
Route::resource('users', UsersController::class)->middleware(['auth']);
Route::resource('follows', UserFollowsController::class)->middleware(['auth']);
Route::resource('likes', LikesController::class)->middleware(['auth']);
Route::resource('likes', LikesController::class)->middleware(['auth']);
Route::post('follows/{follow}', [UserFollowsController::class, 'destroy']);
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home')->middleware(['auth']);
